const BASE_URL = "https://62f1037e25d9e8a2e7c3d659.mockapi.io";

import { getNewTask, renderTask } from "./task.controller.js";

let getDSTask = () => {
  axios({
    url: `${BASE_URL}/Task`,
    method: "GET",
  })
    .then(function (res) {
      console.log("res", res.data);
      renderTask(res.data);
    })
    .catch(function (err) {
      console.log("err", err);
    });
};
getDSTask();

let deleteTask = (id) => {
  axios({
    url: `${BASE_URL}/Task/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log("res", res);
      getDSTask();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
};
window.deleteTask = deleteTask;

let completeTask = (id) => {
  let currentTask;
  axios({
    url: `${BASE_URL}/Task/${id}`,
    method: "GET",
  })
    .then(function (res) {
      currentTask = res.data;

      currentTask.isComplete = true;
      console.log("currentTask: ", currentTask);
      axios({
        url: `${BASE_URL}/Task/${id}`,
        method: "PUT",
        data: currentTask,
      })
        .then(function (res) {
          console.log("currentTask: ", currentTask);
          console.log("res", res);

          getDSTask();
        })
        .catch(function (err) {
          console.log("err: ", err);
        });
    })
    .catch(function (err) {
      console.log("err", err);
    });
};
window.completeTask = completeTask;

document.getElementById("addItem").addEventListener("click", function () {
  let newTask = getNewTask();
  console.log("newTask: ", newTask);
  axios({
    url: `${BASE_URL}/Task`,
    method: "POST",
    data: newTask,
  })
    .then(function (res) {
      console.log("res: ", res);
      document.getElementById("newTask").value = "";
      getDSTask();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
});

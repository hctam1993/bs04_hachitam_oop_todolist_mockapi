export class Task {
  constructor(nameTask) {
    this.id = -1;
    this.nameTask = nameTask;
    this.isComplete = false;
  }
}

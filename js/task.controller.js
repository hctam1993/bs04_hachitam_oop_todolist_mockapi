import { Task } from "./task.modal.js";

export let renderTask = (arr) => {
  let strToDo = "";
  let strComplete = "";
  arr.forEach((item) => {
    console.log("item: ", item);

    if (item.isComplete == false) {
      let str = `<li>
          <span>${item.nameTask}</span>
          <div class="buttons">
          <button class="remove" onclick="deleteTask('${item.id}')"><i class="fa fa-trash-alt"></i></button>
          <button class="complete" onclick="completeTask('${item.id}')"><i class="fa fa-check-circle"></i></i></button>
          </div>
      </li>`;
      strToDo += str;
    } else {
      let str = `<li>
          <span>${item.nameTask}</span>
          <div class="buttons">
          <button class="remove" onclick="deleteTask('${item.id}')"><i class="fa fa-trash-alt"></i></button>
          <button class="complete" onclick="completeTask('${item.id}')"><i class="fa fa-check-circle"></i></i></button>
          </div>
      </li>`;
      strComplete += str;
    }
  });

  document.getElementById("todo").innerHTML = strToDo;
  document.getElementById("completed").innerHTML = strComplete;
};

export let getNewTask = () => {
  let newTask = document.getElementById("newTask").value;
  return new Task(newTask);
};
